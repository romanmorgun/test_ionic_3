import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(public navCtrl: NavController, public admob: AdMobFree) {

  }




    showBanner() {

        let bannerConfig: AdMobFreeBannerConfig = {
            isTesting: true, // Remove in production
            autoShow: true,
            // id: 'ca-app-pub-9588733782602364/7395538472'
        };

        this.admob.banner.config(bannerConfig);

        this.admob.banner.prepare().then(() => {
            // success
            // alert(bannerConfig);
            this.admob.banner.show();
            console.log(bannerConfig);
        }).catch(e => alert(e));

    }

}
